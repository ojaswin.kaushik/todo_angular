import { Component, OnInit, Output,EventEmitter ,Input} from '@angular/core';
import { Todo } from 'src/app/todo';


@Component({
  selector: 'app-add-todo',
  templateUrl: './add-todo.component.html',
  styleUrls: ['./add-todo.component.css']
})
export class AddTodoComponent implements OnInit {
   title?:string;
   Description?:string;
  @Input() todo?:Todo;
  @Output() todoAdd :EventEmitter<Todo> =new EventEmitter();
  constructor() { }

  ngOnInit(): void {
  }
  onSubmit()
  {
    if (!this.title?.split(' ').join('').length)
    {
      alert("Title Box should not be empty")
      return 
    }
    if (!this.Description?.split(' ').join('').length)
    {
      alert("Description Box should not be empty")
      return 
    }
     
    const todo= {
      sno:Math.floor(Math.random()*1000),
      title:this.title.toLowerCase(),
      desc:this.Description.toLowerCase(),
      active:true
      
    }
    //console.log(todo)
    this.todoAdd.emit(todo)
  }
}
