import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { Todo } from 'src/app/todo';


@Component({
  selector: 'app-todo-item',
  templateUrl: './todo-item.component.html',
  styleUrls: ['./todo-item.component.css']
})
export class TodoItemComponent implements OnInit {
  @Input() todo?: Todo;
  @Output() todoDelete: EventEmitter<Todo> = new EventEmitter();
  @Output() todoMark: EventEmitter<Todo> = new EventEmitter();
  public card: any;
  public mark: string="Mark As Done";
  public myid:any;
  public edit: boolean=true;

  x: boolean = true;
  constructor() { }

  ngOnInit(): void {

  }
  onClick(todo: Todo | undefined) {
    this.todoDelete.emit(todo)
    console.log("Delete item pressed and event triggered", todo?.sno)

  }
  
  // Edit Description and Title

  editTodo(event:any)
  
  {
    this.edit=!this.edit
    console.log("is editable",event.target,event.target.readonly)
  }
  
  markAsDone(todo: Todo | any ,isActive:boolean|any) {
    this.mark= !isActive?"Mark As Done" :"Mark as Undone";
  
    this.todoMark.emit(todo)
    
    console.log("Mark item pressed and event triggered", todo?.sno)
    
    
    
  }
  
}
