import { Component, OnInit, Output } from '@angular/core';
import { Todo } from 'src/app/todo';
@Component({
  selector: 'app-todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.css']
})
export class TodosComponent implements OnInit {
  localItem: string | any;
  todos?: Todo[]|any;
  //to hide buttons when no todos are present
  enabled!: boolean;
  findId?:number;
  
  //console.log("ecec");
  
  constructor() {
    this.localItem = localStorage.getItem("todos")
    if (this.localItem === null)
      {
        this.todos = []
        
      }
      
    else {
      this.todos = JSON.parse(this.localItem)
      
    }
    if (!this.todos?.length)
      this.enabled=true
    else
        this.enabled=false
    
  }

  ngOnInit(): void {
  }

  //Find By Id

  findById()
  {
    
    this.localItem = localStorage.getItem("todos")
    this.todos = JSON.parse(this.localItem)
    this.todos = this.todos?.filter((elem: { sno: number | undefined; }) => {
      return elem.sno == this.findId

    })
  }
  //Sort By Id
  sortById()

  {
    this.todos?.sort((a:any,b:any)=>
    {
      return a?.sno-b?.sno
    })
  }
  //Active
  active()
  {
    this.localItem = localStorage.getItem("todos")
    this.todos = JSON.parse(this.localItem)
    this.todos = this.todos?.filter((elem: { active: boolean; }) => {
      return elem.active ===true

    })
  }
  //Completed
  completed()
  {
    this.localItem = localStorage.getItem("todos")
    this.todos = JSON.parse(this.localItem)
    this.todos = this.todos?.filter((elem: { active: boolean; }) => {
      return elem.active ===false

    })
  }
  //View All
  viewAll()
  {
    this.localItem = localStorage.getItem("todos")
    if (this.localItem === null)
      this.todos = []
    else {
      this.todos = JSON.parse(this.localItem)
    }
  }
 //Mark todo as Active
  markTodo(todo:Todo)
  {
    todo.active=!todo.active
    localStorage.setItem("todos", JSON.stringify(this.todos))
    
    
  }

  //Mark All todo As Done
  markAllAsDone()
  {
    this.localItem = localStorage.getItem("todos")
    this.todos = JSON.parse(this.localItem)
    
    this.todos.forEach((elem: any) => {
      elem.active=false
      
    });
  }
  //Delete todo
  deleteTodo(todo: Todo) {
    this.todos = this.todos?.filter((elem: { sno: number | undefined; }) => {
      return elem.sno !== todo.sno

    })
    if (!this.todos?.length)
        this.enabled=true
    localStorage.setItem("todos", JSON.stringify(this.todos))
  }

  //Delete All Completed todo
  deleteAllCompleted()
  {
    this.todos = this.todos?.filter((elem: { active: any; }) => {
      return elem.active

    })
    if (!this.todos?.length)
        this.enabled=true
    localStorage.setItem("todos", JSON.stringify(this.todos))
  }
  //Add Todo

  addTodo(todo: Todo) {

    this.todos?.push(todo)
    console.log(this.todos);
    this.enabled=false
    localStorage.setItem("todos", JSON.stringify(this.todos))
  }

  //Delete All todos

  deleteAll() {
    
    this.todos = []
    localStorage.setItem("todos", JSON.stringify(this.todos))
    this.enabled=true
    
   
  }
}
